#!/bin/bash

taskId=`curl -u admin:$NEXUS_PWD -X GET 'http://localhost:8081/service/rest/v1/tasks' |
awk '$3 ~ "backup" { print f } { f=$0 } ' | sed -n /id/p | awk ' { print $3 } ' |
sed 's/[,"]//g'`
timestamp=`date +"%Y-%m-%d"`
echo $timestamp

curl -u admin:$NEXUS_PWD -v -X POST "http://localhost:8081/service/rest/v1/tasks/${taskId}/run"

docker stop nexus

mkdir -p nexus-backup
docker cp nexus:/tmp nexus-backup
docker cp nexus:/nexus-data/blobs nexus-backup
docker restart nexus
docker exec -u root nexus bash -c "rm -rf /tmp/*"

tar cvf "nexus-backup/nexus-backup-${timestamp}.tar" nexus-backup/tmp nexus-backup/blobs
rm -rf nexus-backup/tmp nexus-backup/blobs