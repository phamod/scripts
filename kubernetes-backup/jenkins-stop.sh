#!/bin/bash


kubectl scale --replicas=1 deployment/jenkins
pod=`kubectl get pods | grep -i jenkins | awk 'NR==1{print $1; exit}'`
# Replace with your Jenkins URL and admin credentials
SERVER="http://localhost:8080"
# File where web session cookie is saved
COOKIEJAR="$(mktemp)"
CREDS="jenkins:admin123"
CRUMB=$(curl -u $CREDS --cookie-jar "$COOKIEJAR" "$SERVER/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,%22:%22,//crumb)")
#curl -X POST -u $CREDS --cookie "$COOKIEJAR" -H "$CRUMB" "$SERVER"/exit
latest_backup=$1

#if the user did not enter a file name use the latest backup
[[ -z $latest_backup ]] && latest_backup=`ls -r jenkins-backup/ | awk 'NR==1{print $1; exit}'`


echo "using file $latest_backup"
tar xf ./jenkins-backup/$latest_backup
kubectl cp  ./jenkins-backup/jenkins_home $pod:/tmp

curl -X POST -u $CREDS --cookie "$COOKIEJAR" -H "$CRUMB" "$SERVER"/exit

kubectl exec $pod -- bash -c "rm -rf /var/jenkins_home/* && mv /tmp/jenkins_home/* /var/jenkins_home/"
kubectl exec $pod -- bash -c "rm -rf /jenkins-backup"
kubectl delete pod $pod