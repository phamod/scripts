#/bin/bash

pod=`kubectl get pods | grep -i jenkins | awk 'NR==1{print $1; exit}'`

echo $pod
timestamp=`date +"%Y-%m-%d"`

mkdir -p jenkins-backup/
kubectl cp $pod:/var/jenkins_home ./jenkins-backup/jenkins_home

tar cvf "jenkins-backup/jenkins-backup-${timestamp}.tar" jenkins-backup/jenkins_home
rm -rf jenkins-backup/jenkins_home