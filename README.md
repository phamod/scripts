# Scripts 
This repository seek to combine scripts to set up a build environment and document code that I feel did not fit a part of the other repositories.

## kubernetes information
### copying files to pod
kubectl cp ./post-receive \<pod name>:/tmp/ -c \<container name>

### persistent storage
all up to date info can be found at [kubernetes](https://kubernetes.io/docs/concepts/storage/volumes/).
- emptyDir lives for the length of the pod and is designed for multi-container pods to share data
- hostPath is meant for debug purposes only in multi node environment there is no guarentee that pods will use the same node so persistent data may be saved to a different node. 
- local like hostPath saves data to a specific node however local uses nodeAffinity to specify the node a pod will run on creating consistency
- nfs allows for sharing a volume across nodes in a linux focused environment
- flexVolume allows for similar functionality as nfs but can use more windows network share like smb 

## script files and uses and caveats
### post-recieve
This is a server side script that gets run after a code is pushed to master or develop branch. This will be used as part of gitlab to trigger a build on jenkins server. more info can be found about the use at [docs.gitlab.com](https://docs.gitlab.com/ee/administration/server_hooks.html) and [git-scm.com](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks). Use the script with gitlab we will place the template script in `/var/opt/gitlab/git-data/repositories/\@hashed/<hashed path>.git/custom_hooks` and modify the username and password to the proper user that gitlab can call build job also changed. If the custom_hooks dir doesn't exist create it. The job url can be found by navigating to the job on jenkins and appending /build to the end. The hashed path can be found in gitlab with the following steps:
- On the login page select the admin area icon the wrench(must be an admin) ![admin area](./images/gitlab/admin-area.png)
- In the admin area select Projects to find the project ![projects](./images/gitlab/projects.png)
- Select the desired project and the path can be found at the Gitaly relative path ![project info](./images/gitlab/project-info.png)

 The script follows the steps found at [cloudbees](https://support.cloudbees.com/hc/en-us/articles/219257077-CSRF-Protection-Explained) to initiate the build using the security built into jenkins.

## Nexus Information
### using a debian repository
* add the following line to /etc/apt/sources.list and replace existing repos for offline use. the release of the container can be found with 
> echo /etc/os-release  
> echo "deb [arch=amd64] http://\<host ip>/repository/apt-\<release>/ \<release> main" > /etc/apt/sources.list
* afterwards a pull of the public gpg key is necessary
> curl -fsSL http://\<host ip>/repository/files/keys/\<release>.gpg.key | apt-key add -  
apt-get update
* now all packages should be available that have been uploaded to the repository

### creating a blob
* In the admin area select Blob Stores > Create blob store
* Give name indicating use for easy use

### create raw file store
* create a new blob for raw files
* in the admin area select repositories > create repository > raw(hosted)

### creating a hosted apt repository
* create a new blob for each debian distribution
* in the admin area select repositories > create repository > apt(hosted)
* give the repo a name indicating distribution I use apt-\<distribution> for my upload script. A new blob might be good to seperate
* run the following commands to create a gpg key for the repo the image must have gpg or be connected and run 'apt-get install gpg'
> docker run -it --name temp localhost:8082/debian  
gpg --gen-key
* The input doesn't really matter and password is not needed for the current use. use the following command to get id of the key
> gpg --list-keys
* using the id of the key create public and private
> gpg --armor --output public.gpg.key --export \<gpg key Id>  
gpg --armor --output private.gpg.key --export-secret-key \<gpg key Id>  
cat private.gpg.key
* use the output of the private key to create the repo for the desired distribution and create repoistory
* copy the public key to host and upload to the raw file repo with name like \<release>.gpg.key
> docker cp temp:/public.gpg.key ./ 
* I put the keys in a directory called keys to contain all gpg keys

### pulling apt dependencies for offline use
* In a container connected to the internet run apt-get install for all packages desired
* after all packages have been downloaded and the container behaves generally how is intended run the following command
> dpkg -l | grep "^ii"| awk '{print $2}'| xargs apt-get -y install --reinstall --download-only
* the resulting files can be found at /var/cache/apt/archives use the command to move outside of the container
> docker cp temp:/var/cache/apt/archives ./

### uploading apt dependencies to nexus
dependencies should only be pushed to a repo of the same distribution or there will be a problem pulling and updating containers that use the repo.
* after the files have been pulled run the following command to upload to a repo if using my naming scheme. this uploads all deb files in the current directory
> upload.sh \<distribution>

## Gitlab Information
### Initial Docker setup
* run the following command forwarding to port 443 is optional for if ssl is enabled for the server
> docker run -d -p 443:443 -p 80:80 -p 22:22 --name gitlab --restart always -v gitlab-config:/etc/gitlab -v gitlab-logs:/var/log/gitlab -v gitlab-data:/var/opt/gitlab --hostname \<name of the host machine> gitlab/giltab-ce:/latest
* gitlab will take a few minutes to start and once it does navigate to the http://\<hostname>/
* on the initial page you will be prompted to enter a new password for 'root'

### Initial kubernetes setup
* apply kubernetes scripts for gitlab
> TODO provide links to scripts 
* gitlab will take a few minutes to start and once it does navigate to the http://\<hostname>/
* on the initial page you will be prompted to enter a new password for 'root'

### Creating a group
groups are a good way to organize projects and restrict people to a group projects quickly without having to access to a bunch of projects
* create by selecting groups and clicking on your groups this will bring you to the groups page. here click on new group
![create group](./images/gitlab/create_group.png)
* on the next page all that is needed is a group name since the url will fill automatically and private is the best option since anyone create an account.
![new group](./images/gitlab/new_group.png)

### creating a project
* after creating a group select that group and on the group page selcet new project (if the group doesn't exeist or the project should be in another group it can be moved later)
![create project](./images/gitlab/create_project.png)
* just like the group, only the name is needed to create the page and the url is populated
* after the project is created follow the steps above for post-receive script to setup push to run the pipeline whenever new code is added to certain branches(develop and master)

### Repairing the database  
Run the following commands on the perspective container
- gitlab-rails c
- settings = ApplicationSetting.last
- settings.update_column(:runners_registration_token_encrypted, nil)  

## Jenkins Information
### Initial Docker setup
* start docker image
> docker run -d -p 8080:8080 -p 50000:50000 --name jenkins --restart always -v jenkins-home:/var/jenkins_home localhost:8082/jenkins:latest
* to find the inital password for admin run the following command
> docker exec jenkins bash -c "cat /var/jenkins_home/secrets/initialAdminPassword"

### Initial kubernetes setup
* apply kubernetes scripts for jenkins
> TODO provide links to scripts
* In order to find the name of the pod execute the following command
> kubectl get pods | grep jenkins
* to find the initail password for admin run the following command
> kubectl exec \<jenkins pod> --  bash -c "cat /var/jenkins_home/secrets/initialAdminPassword"

### Initial setup

* after jenkins starts go to the webpage at port 8080 forexample http://localhost:8080 and see the initial login. Use the initial password from the previous setup step taken
![initial_login](./images/jenkins/initial_login.png)
* If connected to the internet select install suggested plugins
![initial_setup](./images/jenkins/inital_setup.png)
* create the inital jenkins admin account that will be used for most setup and configuration
![create_admin](./images/jenkins/create_admin.png)
* when setting the jenkins url use the following depending on setup if using docker with default network http://172.17.0.1:8080/ will route to the instance of jenkins otherwise kubernetes jenkins uses http://jenkins:8080/ to route to the page within the cluster
![jenkins_url](./images/jenkins/jenkins_url.png)
* click finish and the inital setup for jenkins has been complete

### Creating a project pipeline
* on the main page select the folders until you have reached the destination where you would like to store the pipeline.
* To create a pipline or a folder select new item in the top right corner
![new_item](./images/jenkins/new_item.png)
* from this page you can now create as many folders as needed until you have the proper organization
* on this page you can now select the multibranch pipeline to create a pipeline that will be used when a branch pushed into develop or master when code is complete
![create_pipeline](./images/jenkins/create_pipeline.png)
* once on the configuration page most settings are for preference but in the branch source select git
![branch_source](./images/jenkins/branch_source.png)
* use the following settings after selecting git to filter names of master and develop branches and provide the http url to the git project
![git_source](./images/jenkins/git_source.png)
* if there is no credential create one using username and password if using http for gitlab
![new credentials](./images/jenkins/new_key.png)
* after the project linked the project needs a Jenkinsfile in the root directory of the project to go through a pipeline. an Example is included in this project

## Sonarqube Information
### initial docker setup
* two containers are needed to properly persist data for this starting with the database we use postgres
* starting the postgres database we use the following command to start it up. more info can be found at [docker](https://hub.docker.com/_/postgres/)
> docker run -d --restart always -e POSTGRES_PASSWORD=postgres -p 5432:5432 -v sonar-postgres-data:/var/lib/postgresql --name sonarqube-postgresql postgres
* the default schema and username are postgres when starting up. the following command will start up sonarqube with connection to that default schema.
> docker run -d --restart always -p 9000:9000 -e SONARQUBE_JDBC_URL=jdbc:postgresql://172.17.0.1/postgres --name sonarqube -e SONARQUBE_JDBC_USERNAME=postgres -e SONARQUBE_JDBC_PASSWORD=postgres sonarqube
* the iniial user is admin with password admin when accessing http://localhost:9000

### initial kubernetes setup
* execute the following command to create deployments for postgres and sonarqube
> TODO add command to apply scripts
* the iniial user is admin with password admin http://localhost:9000

### Adding sonarqube to the pipeline
* all projects must have a Jenkinsfile and a sonar-project.properties examples of how the two are included.
* within a jenkins pipeline host must be provided and either a token or username and password to the sonar scanner container
* within the sonar-project.properties two main properties are sonare.projectKey which is the name of the project on sonarqube and a reportPaths corresponding to the language used. the reports path points to an xml file.  