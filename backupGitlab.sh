#!/bin/bash

timestamp=`date +"%Y-%m-%d"`
echo $timestamp

docker exec gitlab bash -c "gitlab-ctl backup-etc && gitlab-backup create"
docker exec gitlab bash -c "rm -rf /tmp/*"
docker exec gitlab bash -c "mv /etc/gitlab/config_backup/* /tmp && mv /var/opt/gitlab/backups/* /tmp"

mkdir -p gitlab-backup

docker cp gitlab:/tmp gitlab-backup
tar cvf "gitlab-backup/gitlab-backup-${timestamp}.tar" gitlab-backup/tmp
rm -rf gitlab-backup/tmp