#!/bin/bash
# upload.sh
# uploads all tgz files to the npm repo of the local nexus repository
# this includes duplicates in case they have different url paths
usage="requires an argument"
[ -z $1 ] && echo $usage && exit 0

if [[ $1 == "npm" ]] 
then
  for f in *.tgz*; do
    curl -u admin:$NEXUS_PWD -X POST "http://localhost:8081/service/rest/v1/components?repository=npm" -F "npm.asset=@$f" -v
  done
fi

if [[ $1 == "stretch"  || $1 == "buster" ]]; 
then
  echo "starting loop"
  for f in *.deb*; do
    curl -u "admin:$NEXUS_PWD" -H "Content-Type: multipart/form-data" --data-binary "@./$f" "http://localhost:8081/repository/apt-$1/"
  done
fi